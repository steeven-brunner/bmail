# -*- coding: utf-8 -*

import poplib
import imaplib
import email
import string
import sys

from Tkinter import *

fenetre = Tk()

champ_label = Label(fenetre, text="Email")
champ_label.pack()
value = StringVar() 
value.set("Email")
entree_mail = Entry(fenetre, textvariable="EMAIL", width=30)
entree_mail.pack()

champ_label = Label(fenetre, text="Password")
champ_label.pack()
value = StringVar() 
value.set("Password")
entree_pass = Entry(fenetre, textvariable="PASS", width=30, show="*")
entree_pass.pack()

champ_label = Label(fenetre, text="Serveur de messagerie")
champ_label.pack()
value = StringVar() 
value.set("Serveur")
entree_serveur = Entry(fenetre, textvariable="SERVEUR", width=30)
entree_serveur.pack()

protoc_pop= IntVar()
protoc_imap= IntVar()
pop_button=Checkbutton(fenetre, text="pop ", variable=protoc_pop).pack()
imap_button=Checkbutton(fenetre, text="Imap", variable=protoc_imap).pack()

def print_body(nb, mailserver):
    fenetre3 = Tk()
    if protoc_pop.get() == 1:
        numMessages = len(mailserver.list()[1])
        for i in reversed(range(numMessages)):
            msg = mailserver.retr(i+1)
            str = string.join(msg[1], "\n")
            mail = email.message_from_string(str)
            if i == nb:            
                message = get_body(mail).decode('utf8') + "\n"
                champ_label = Label(fenetre3, text=message).pack(side=BOTTOM, padx=100, pady=50)
    if protoc_imap.get() == 1:
        (retcode, messages) = mailserver.search(None, '(UNSEEN)')
        if retcode == 'OK':
            
            for num in messages[0].split() :
                if nul == nb:
                    original = email.message_from_string(response_part[1])
                    message = get_body(mailserver).decode('utf8') + "\n"
                    champ_label = Label(fenetre3, text=message).pack(side=BOTTOM, padx=100, pady=50)  
    
        (retcode, messages) = mailserver.search(None, '(SEEN)')
        if retcode == 'OK':
            
            for num in messages[0].split() :
                if nul == nb:
                    original = email.message_from_string(response_part[1])
                    message = get_body(mailserver).decode('utf8') + "\n"
                    champ_label = Label(fenetre3, text=message).pack(side=BOTTOM, padx=100, pady=50)  

def print_header(header, fenetre, nb, mailserver):
    
    Button(fenetre, text='     Delete     ', command = lambda: delete_mail(nb, fenetre)).pack(side=BOTTOM, padx=5, pady=5)
    Button(fenetre, text=header, command= lambda: print_body(nb, mailserver)).pack(side=BOTTOM, padx=5, pady=5)

def get_body(email_message_instance):
    maintype = email_message_instance.get_content_maintype()
    if maintype == 'multipart':
        for part in email_message_instance.get_payload():
            if part.get_content_maintype() == 'text':
                return part.get_payload().decode('utf8')
            elif maintype == 'text':
                return email_message_instance.get_payload().decode('utf8')

def chose_protocol():
    if protoc_pop.get() == 1:
        pop_display()
    if protoc_imap.get() == 1:
        imap_display()
                
def delete_mail(nb, fenetre):
    print "nb = "
    print nb
    mail = imaplib.IMAP4_SSL(entree_serveur.get())
    (retcode, capabilities) = mail.login(entree_mail.get(), entree_pass.get())
    mail.list()
    mail.select('inbox')
    
    mail.store(nb, '+FLAGS', '\\DELETED')

    mail.expunge()

    if protoc_pop.get() == 1:
        pop_display()
    if protoc_imap.get() == 1:
        imap_display()

    
def pop_display():
    mailserver = poplib.POP3_SSL(entree_serveur.get())
    mailserver.user(entree_mail.get()) #use 'recent mode'                           
    mailserver.pass_(entree_pass.get()) #consider not storing in plaintext!                          

    mail_stat = mailserver.stat();

    number_of_mails = mail_stat[0]
    numMessages = len(mailserver.list()[1])
    message = "\nYou'll read emails using pop3 protocol"
    message += "There is %d email(s) in your box  \n" % number_of_mails
    
    fenetre2 = Tk()
    for i in reversed(range(numMessages)):
        message = "-------------------------------------------------\n"
        msg = mailserver.retr(i+1)
        str = string.join(msg[1], "\n")
        mail = email.message_from_string(str)

        message += "From: " + mail["From"].decode('utf8') + "\n"
        message += "Subject: " + mail["Subject"] + "\n"
        message += "To: " + mail["To"] + "\n"
        message += "Date: " + mail["Date"] + "\n"

        print_header(message, fenetre2, i, mailserver)


def imap_display():
    mail = imaplib.IMAP4_SSL(entree_serveur.get())
    (retcode, capabilities) = mail.login(entree_mail.get(), entree_pass.get())
    mail.list()
    mail.select('inbox')
    
    fenetre2 = Tk()
    (retcode, messages) = mail.search(None, '(UNSEEN)')
    if retcode == 'OK':

        for num in messages[0].split() :
            message =  "\n\n******...Processing...Imap protocol...******\n\n"
            typ, data = mail.fetch(num,'(RFC822)')
            for response_part in data:
                if isinstance(response_part, tuple):
                    message = "---------------------------------------------\n"
                    original = email.message_from_string(response_part[1])

                    message += "From: " + original['From'] + "\n"
                    message += "Subject: " + original['Subject'] + "\n"
                    message += "To: " + original['To'] + "\n"
                    message += "Date: " + original['Date'] + "\n"

                    print_header(message, fenetre2, num, mail)

    (retcode, messages) = mail.search(None, '(SEEN)')
    if retcode == 'OK':

        for num in messages[0].split() :
            typ, data = mail.fetch(num,'(RFC822)')
            for response_part in data:
                if isinstance(response_part, tuple):
                    message2 = "---------------------------------------------\n"
                    original = email.message_from_string(response_part[1])

                    message2 += "From: " + original['From'] + "\n"
                    message2 += "Subject: " + original['Subject'] + "\n"
                    message2 += "To: " + original['To'] + "\n"
                    message2 += "Date: " + original['Date'] + "\n"

                    print_header(message2, fenetre2, num, mail)
                
Button(fenetre, text ='Valider', command=chose_protocol).pack(side=TOP, padx=5, pady=5)
Button(fenetre, text ='Close', command=quit).pack(side=BOTTOM, padx=5, pady=5)
fenetre.mainloop()
